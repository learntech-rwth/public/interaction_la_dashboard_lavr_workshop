# Learning Technologies SS23

## Group 2

- Ana Dragoljic
- Lars Florian Meiendresch
- Marc Troll

## Research Questions

1. In which aspects differ the learning experiences when using Virtual Reality instead of 2D desktop application?
2. In which aspects differ the learning experiences for interactive scenarios instead of behaviouristic informatic texts?

## Web Dashboard

Our web dashboard is interactive and contains many indicators to investigate different characteristics.

### Analyzed Data

We analyzed all xAPI statements from the big learning technologies VR study from 2022 (**4,75 GB**) and the affiliated post-survey data.

### Frameworks

- (Pre-)Processing of Data: [Jupyter Notebooks](https://jupyter.org/), [NumPy](https://numpy.org/)
- Web-Dashboard: [VueJS](https://vuejs.org/), [ApexCharts.js](https://apexcharts.com/)

### Run Web Dashboard

```
cd dashboard-app && npm install && npm run dev
```
