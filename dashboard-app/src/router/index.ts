import { createRouter, createWebHistory } from 'vue-router'
import Overview from "@/pages/overview.vue";
import Position from "@/pages/position.vue";
import Comparison from "@/pages/comparison.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/overview',
      component: Overview
    },
    {
      path: '/position',
      component: Position
    },
    {
      path: '/compare',
      component: Comparison
    },
    {
      path: '/:pathMatch(.*)*',
      redirect: "/overview"
    }
  ]
})

export default router
